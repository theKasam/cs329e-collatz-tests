#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, find_steps, divide_collatz

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1)

    def test_read_3(self):
        s = "10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(10, 10)
        self.assertEqual(v, 7)

    # -----
    # steps
    # -----

    def test_steps1(self):
        l = find_steps(657, 7004)
        self.assertEqual(l, [1000, 7000])

    def test_steps2(self):
        l = find_steps(3000, 6900)
        self.assertEqual(l, [3000, 6000])

    def test_steps3(self):
        l = find_steps(952040, 968900)
        self.assertEqual(l, [953000, 968000])

    def test_steps4(self):
        l = find_steps(3500, 4500)
        self.assertEqual(l, [4000, 4000])

    def test_steps5(self):
        l = find_steps(4000, 4000)
        self.assertEqual(l, [4000, 4000])

    def test_steps6(self):
        l = find_steps(996234, 999999)
        self.assertEqual(l, [997000, 999000])

    # ------
    # divide
    # ------

    def test_divide_1(self):
        v = divide_collatz(657, 7004)
        self.assertEqual(v, 262)

    def test_divide_2(self):
        v = divide_collatz(4000, 5000)
        self.assertEqual(v, 215)

    def test_divide_3(self):
        v = divide_collatz(888333, 999887)
        self.assertEqual(v, 507)

    def test_divide_4(self):
        v = divide_collatz(5000, 5000)
        self.assertEqual(v, 29)

    def test_divide_5(self):
        v = divide_collatz(44, 300022)
        self.assertEqual(v, 443)

    def test_divide_6(self):
        v = divide_collatz(123, 453234)
        self.assertEqual(v, 449)

    def test_divide_7(self):
        v = divide_collatz(3001, 5000)
        self.assertEqual(v, 238)

    def test_divide_8(self):
        v = divide_collatz(4000, 5998)
        self.assertEqual(v, 236)

    def test_divide_9(self):
        v = divide_collatz(1, 10)
        self.assertEqual(v, 20)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 10, 10, 7)
        self.assertEqual(w.getvalue(), "10 10 7\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("10\n100 200\n201 210\n900 1000 174\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 10 7\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_3(self):
        r = StringIO("10\n200 100\n201 210\n1000 900 174\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 10 7\n200 100 125\n201 210 89\n1000 900 174\n")
# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage-3.5 run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage-3.5 report -m                   >> TestCollatz.out



% cat TestCollatz.out
.............................
----------------------------------------------------------------------
Ran 29 tests in 0.741s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          62      0     38      1    99%   54->71
TestCollatz.py     108      0      0      0   100%
------------------------------------------------------------
TOTAL              170      0     38      1    99%
