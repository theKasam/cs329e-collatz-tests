#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 5\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 5)
    
    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)
    
    def test_read_3(self):
        s = "2000 21000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  2000)
        self.assertEqual(j, 21000)
    

    def test_eval_1(self):
        v = collatz_eval(1, 5)
        self.assertEqual(v, 8)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(2000, 21500)
        self.assertEqual(v, 279)

    def test_eval_4(self):
        v = collatz_eval(1500, 82500)
        self.assertEqual(v, 351)


    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 5, 8)
        self.assertEqual(w.getvalue(), "1 5 8\n")
    def test_print(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")
    def test_print(self):
        w = StringIO()
        collatz_print(w, 2000, 21000, 279)
        self.assertEqual(w.getvalue(), "2000 21000 279\n")
    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 5\n100 200\n201 210\n900 10002\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 5 8\n100 200 125\n201 210 89\n900 10002 262\n")
    
    def test_solve(self):
        r = StringIO("10 5050\n1 2\n150 450\n9000 10000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 5050 238\n1 2 2\n150 450 144\n9000 10000 260\n")
    
    def test_solve(self):
        r = StringIO("1 160\n100 2000\n7 13\n2400 37500\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 160 122\n100 2000 182\n7 13 20\n2400 37500 324\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage-3.5 run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage-3.5 report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
